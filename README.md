Natural Language Processing project from school

- Uses KNN algorithm to retrieve words from the 100 vocab list to create analogy based on word vectors

- Used Microsoft Azure S14 Ubuntu instance for performance (16 CPUs and 112GB RAM)

Unique hits:  125748
Time Taken:  197.27888  seconds