#!/usr/bin/python
import sys
import numpy as np
import time

def readVocab(fin):
    return ([word.strip() for word in fin])

def readWordVectors(fin, vocab):
    word_vectors = {}
    header = fin.readline()
    vocab_size, vector_size = map(int, header.split())
    binary_len = np.dtype('float32').itemsize * vector_size
    print 'All vocab size: ', vocab_size
    print 'Our vocab size: ', len(vocab)
    print 'Vector size   : ', vector_size

    for line in xrange(vocab_size):
        word = []
        while True:
            ch = fin.read(1)
            if ch == b' ':
                word = ''.join(word)
                break
            if ch != b'\n':
                word.append(ch)

        if vocab and word in vocab: word_vectors[word] = np.fromstring(fin.read(binary_len), dtype='float32')
        else: fin.read(binary_len)

    return word_vectors

def getCosineSimilarity(v1, v2):
    num  = np.dot(v1, v2)
    den1 = np.sqrt(np.dot(v1, v1))
    den2 = np.sqrt(np.dot(v2, v2))
    return num / (den1 * den2)

def getSimilarities(wv, v1):
    l = [(getCosineSimilarity(v1, v2), w2) for (w2, v2) in wv.items()]
    return sorted(l, reverse=True)

VOCAB_FILE = sys.argv[1] # vocab.txt
W2V_FILE = sys.argv[2]   # w2v.bin
K = 5
out = open("output.txt", "w") # outputs the results to a file

start = time.clock() # timer
ucount = 0

vocab = readVocab(open(VOCAB_FILE))
wv = readWordVectors(open(W2V_FILE), vocab)

for a in range(len(vocab)):
	print "index: ",a, "    head word: ",vocab[a]
	l = getSimilarities(wv, wv[vocab[a]])
	b = a+1
	max = 0
	while max != 98:
		if b == 100: # if index of b is 100, restart at 0
			b = 0
		count = 0
		for c in range(K):
			if vocab[a]==vocab[b] or vocab[a]==l[c][1] or vocab[b]==l[c][1]:continue #if there are same words, skip
			lastcount = 0
			for d in range(K):
				if lastcount == 5: break # if K hits 5, break off the loop
				ans = getSimilarities(wv, wv[vocab[a]] + wv[vocab[b]] - wv[l[c][1]])
				if vocab[a]==vocab[b] or vocab[a]==l[c][1] or vocab[a]==ans[d][1] or vocab[b]==l[c][1] or vocab[b]==ans[d][1] or l[c][1]==ans[d][1]:continue #if there are same words, skip
				print vocab[a],' : ',vocab[b],' = ', l[c][1],' : ', ans[d][1] # print to terminal
				print >>out, vocab[a],' : ',vocab[b],' = ', l[c][1],' : ', ans[d][1] # print & append to output file
				lastcount+=1
				ucount+=1
			count+=1
		b+=1
		max+=1
print "Done - Please find \'output.txt\' file for more information"
end = time.clock()
print >>out, "\nUnique hits: ",ucount, "hits"
print >>out, "Time Taken: ",end-start," seconds"
out.close()